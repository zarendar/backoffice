'use strict';

import Reflux from 'reflux';

const Actions = Reflux.createActions([
  //modal
  'showModal',
  'hideModal',
  //end modal
  //session
  'init',
  'signIn',
  'signUp',
  'logout',
  //end session
  //settings
  'initSettings',
  'initCertificates',
  'submitSettings',
  'submitPersonalData',
  'submitSettingsImg',
  'submitSettingsCertificate',
  'profileCover',
  'avatar',
  'certificate',
  'removeText',
  //emd settings
  //events
  'initEvents',
  'createEvent',
  'editEvent',
  //end events
  'eventAction',
  'removeAction',
  'createWorkout',
  'initWorkouts',
  'initSessions',
  'newWorkoutLocation',
  'editSession',
  'dragSession',
  'changeWorkout',
]);

export default Actions;