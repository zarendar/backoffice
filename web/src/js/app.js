'use strict'

import React from 'react';
import Reflux from 'reflux';

import Actions from './actions/actions';
import CalendarStore from './stores/calendarStore';
import SessionStore from './stores/sessionStore';
import ModalStore from './stores/modalStore';

import { Url } from './util/constants';

import Header from './components/header';
import Login from './components/login';
import Modal from './components/modal';
import CreateEvent from './components/createEvent';
import CreateWorkout from './components/createWorkout';
import CreateWorkoutLocation from './components/createWorkoutLocation';
import EditEvent from './components/editEvent';
import EditSession from './components/editSession';

const App = React.createClass({
    mixins: [
      Reflux.listenTo(CalendarStore, 'onCalendarUpdate'),
      Reflux.listenTo(SessionStore, 'onSessionUpdate'),
      Reflux.listenTo(ModalStore, 'onModalUpdate'),
    ],
    getInitialState() {
        return {
            dropData: CalendarStore.getDefaultData(),
            session: SessionStore.getDefaultData(),
            modal: ModalStore.getDefaultData()
        };
    },
    onCalendarUpdate(newCalendarState) {
        this.setState({ dropData: newCalendarState });
    },
    onSessionUpdate(newSessionState) {
        this.setState({ session: newSessionState });
    },
    onModalUpdate(newModalState) {
      this.setState({ modal: newModalState });
    },
    componentWillMount() {
      const isInit = this.state.session.isInit;
        Actions.init(true);
    },
    hideModal(e) {
      if (e) { e.preventDefault(); }
      Actions.hideModal();
    },
    getModalComponent(modal) {
      // if (!modal.type) {
      //     return null;
      // }

      if (modal.type) $('#myModal').modal('show');

      let modalInner = null;
      const modalProps = {
          data: this.state.modal.data,
      };

      const location = {
          location: this.state.modal.location,
      };

      switch (modal.type) {
          case 'createEvent':
              modalInner = <CreateEvent  />; break;
          case 'createWorkout':
              modalInner = <CreateWorkout  />; break;
          case 'editEvent':
              modalInner = <EditEvent {...modalProps} />; break;
          case 'editSession':
              modalInner = <EditSession {...modalProps} />; break;
          case 'createWorkoutLocation':
              modalInner = <CreateWorkoutLocation {...location} />; break;

      }
       return (
          <Modal hideModal={ this.hideModal }>
              { modalInner }
          </Modal>
      );
    },
    render() {
      const session = this.state.session;
      const isSigned = session.isSigned;
      const isLoaded = session.isLoaded;
      const modal = this.state.modal;
      let appContent;
      let loader;


      if (!isLoaded) {
        loader = (
          <div className="loader">
            <img src="src/img/preloader.gif" alt=""/>
          </div>
        )
      }

      if (isSigned) {
        appContent = (
          <div className="isLogin">
            <Header />
            <div className="content">
              {this.props.children}
            </div>
          </div>
        )
      } else {
        appContent = (
          <div className="login-wrapper">
            <Login errorMessage={session.errorMessage}/>
          </div>
        )
      }

      return(
        <div className="app">
          {loader}
          {appContent}
          { this.getModalComponent(modal) }
        </div>
      );
    }
});

export default App;