import React from 'react';
import { Link } from 'react-router';

const AccountNavigation = React.createClass({
    render() {
      return(
        <div className="account-navigation">
          <div className="container">
            <ul className="row list-unstyled list-inline">
              <li>
                <Link to="/account/settings" activeClassName="active" >
                  <i className="icon-settings"></i>
                  <span>Settings</span>
                </Link>
              </li>
              <li>
                <Link to="/account/workouts" activeClassName="active" >
                  <i className="icon-sneakers"></i>
                  <span>Workouts</span>
                </Link>
              </li>
              <li>
                <Link to="/account/events" activeClassName="active" >
                  <i className="icon-drop"></i>
                  <span>Events</span>
                </Link>
              </li>
            </ul>
          </div>
        </div>
      );
    }
});


export default AccountNavigation;