import React from 'react';
import { Link, IndexLink } from 'react-router';

const AccountTabMenu = React.createClass({
    render() {
      return(
        <div className="account-tab-menu">
          <div className="container">
            <div className="row">
              <div className="col-xs-12 col-md-10 col-md-offset-1">
                 <div className="tab-menu-container">
                  <ul className="list-unstyled">
                    <li>
                      <IndexLink
                        to="/account/settings"
                        activeClassName="active"
                        className="btn btn-default">
                        Personal Data
                      </IndexLink>
                    </li>
                    <li>
                      <Link
                        to="/account/settings/socialNetworks"
                        activeClassName="active"
                        className="btn btn-default">
                        Social Networks
                      </Link>
                    </li>
                     <li>
                      <Link
                        to="/account/settings/settings"
                        activeClassName="active"
                        className="btn btn-default">
                        Settings
                      </Link>
                    </li>
                     <li>
                      <Link
                        to="/account/settings/certificates"
                        activeClassName="active"
                        className="btn btn-default">
                        Certificates
                      </Link>
                    </li>
                    <li>
                      <Link
                        to="/account/settings/images"
                        activeClassName="active"
                        className="btn btn-default">
                        Images
                      </Link>
                    </li>
                  </ul>
                 </div>
              </div>
            </div>
          </div>
        </div>
      );
    }
});


export default AccountTabMenu;