import React from 'react';
import Actions from '../actions/actions';

import Moment from 'moment';
import Draggable from 'jquery-ui/draggable';
import Droppable from 'jquery-ui/droppable';
import FullCalendar from 'fullcalendar/dist/fullcalendar.js';



const Calendar = React.createClass({
  getInitialState() {
      return {
          workouts: []
      };
  },
  hideModal() {
    Actions.hideModal();
  },
  componentWillReceiveProps(nextProps) {
    //  ADD EVENTS TO CALENDAR FROM API


  },
  componentDidMount() {


    const sessions = this.props.data.data;
    const workouts = this.props.data.addedWorkouts;


    var isEventOverDiv = function(x, y) {
      var external_events = $( '#external-events' );
      var offset = external_events.offset();
      offset.right = external_events.width() + offset.left;
      offset.bottom = external_events.height() + offset.top;
      if (x >= offset.left
          && y >= offset.top
          && x <= offset.right
          && y <= offset .bottom) { return true; }
      return false;
    }

    var sendAction = function(start, end, day, id){
      day = day - 1;
      if(day == -1){ day = 6};
      Actions.eventAction(start, end, day, id);
    }
    var droped_event_duration;
    /* initialize the calendar
    -----------------------------------------------------------------*/
    $('#calendar').fullCalendar({
        // put your options and callbacks here
        defaultView: 'agendaWeek',
        allDaySlot: false,
        slotDuration: '0:15:00',
        forceEventDuration: true,
        defaultTimedEventDuration: '0:15:00',
        minTime: '09:00:00',
        maxTime: '20:00',
        slotEventOverlap: false,
        header: false,
        columnFormat: 'ddd',
        dragRevertDuration: 0,
        ignoreTimezone: false,
        editable: true,
        droppable: true,
        height: 615,
        eventOverlap: false,
        eventDrop: function(event){
          var day = parseInt(event.start.format("d"));
          day = day - 1;
          if(day == -1){ day = 6};
          Actions.changeWorkout(event.session_id, day, event.start.format("h m a"), event.location_id, event.workout_id);
        },
        drop: function(date, jsEvent, ui){
          var duration = $(this).find('.duration').find('span')[0].innerText;
          droped_event_duration = parseInt(duration);
          
        },
        eventDragStop: function(event, jsEvent, ui, view ) {
          //alert('stop')
          if(isEventOverDiv(jsEvent.clientX, jsEvent.clientY)) {
            $('#calendar').fullCalendar('removeEvents', event._id);
            Actions.removeAction(event._id);
          }
        },
        eventReceive: function (event, view) {
          event.start._isUTC = false;
          event.end._isUTC = false;
          event.start._ambigZone = false;
          event.end._ambigZone = false;
          var hours = Math.floor(droped_event_duration/60);
          var minutes = droped_event_duration%60;
          event.end.hour(event.start.hour()+hours);
          event.end.minutes(event.start.minutes()+minutes);
          $('#calendar').fullCalendar('renderEvent', event, true);
          var drop_enabled = true;
          event.start.subtract(2, 'hours');
          event.end.subtract(2, 'hours');
          var events_list = $('#calendar').fullCalendar('clientEvents');
          events_list.forEach(function(element){
           if(element.start.day() == event.start.day() && element != event && (Math.abs(event.start.diff(element.start, 'minutes')) < event.end.diff(event.start, 'minutes') || Math.abs(event.end.diff(element.end, 'minutes')) < event.end.diff(event.start, 'minutes'))){
             drop_enabled = false;
            }
          });
          if(drop_enabled){   
            $('#calendar').fullCalendar('renderEvent', event, true);
            Actions.showModal('createWorkoutLocation', null, this.props.data.locations);
            sendAction(event.start.format("h m a"), event.end.format("h m a"), event.start.format("d"), event._id);
          } else {
            $('#calendar').fullCalendar('removeEvents', event._id);
          }
        }.bind(this),
    })


    sessions.forEach(function(workout){
      workouts.forEach(function(added_element){
        if(workout.id == added_element.session_id){
          var start_time = new Moment();
          var day = parseInt(added_element.week_day) + 1;
          if(day == 7){day = 0};

          start_time.day(day);
          start_time.hours(added_element.start_time.split(':')[0]);
          start_time.minutes(added_element.start_time.split(':')[1]);
          start_time.seconds(0);
          start_time._fullCalendar = true;
          var end_time = new Moment();
          end_time.day(day);
          end_time.hours(added_element.end_time.split(':')[0]);
          end_time.minutes(added_element.end_time.split(':')[1]);
          end_time.seconds(0);
          end_time._fullCalendar = true;
          var ev = {title: workout.ui_label, start: start_time, end: end_time, allDay: false};
          ev.workout_id = added_element.id;
          ev.session_id = added_element.session_id;
          ev.location_id = added_element.location_id;
          $('#calendar').fullCalendar('renderEvent', ev);
        }
      });
    });

  },
  render() {
    return(
      <div id="calendar" className="calendar"></div>
    );
  }
});

export default  Calendar;