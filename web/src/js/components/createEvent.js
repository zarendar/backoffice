import React from 'react';
import { Form } from 'formsy-react';
import Actions from '../actions/actions';
import ModalInput from './modalInput';
import ModalTextarea from './modalTextarea';


const Modal = React.createClass({
	getInitialState() {
	    return {
	        notify: false
	    };
	},
	submit(data) {
	  Actions.createEvent(data);
	},
	notifyFormError() {
		this.setState({notify: true});
	},
    hideModal() {
      Actions.hideModal();
    },
  render() {
  	const notify = (this.state.notify) ? 'please fill all fields' : null;
    return(
  	<div className="modal-content">
        <div className="modal-header">
            <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.hideModal}>
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 className="modal-title">Create New Event</h4>
        </div>
		<div className="modal-body">
			<Form onValidSubmit={this.submit} onInvalidSubmit={this.notifyFormError}>
				<div className="create-new-event">
					<ModalInput
	                    name="ui_label"
	                    pl="Label"
	                    block="true"
	                    required />

			      	<div className="row">
			      		<div className="col-sm-8 duration">
							<label>Duration</label>
							<ModalInput
								type="number"
		                        name="hours"
		                        pl="0"
		                        required
		                        /> h.
							<ModalInput
								type="number"
		                        name="min"
		                        pl="0"
		                        required
		                        /> min.
			      		</div>
			      		<div className="col-sm-4 price">
							<label htmlFor="price">Price</label>
							<ModalInput
		                        name="price"
		                        pl=""
		                        required
		                        />
							<span className="currency">$</span>
			      		</div>
			      	</div>

			      	<div className="row">
			      		<div className="col-sm-6 date">
			      			<label>Date</label>
			      			<ModalInput
								type="number"
		                        name="day"
		                        pl="Day"
		                        required
		                        />
		                    <ModalInput
								type="number"
		                        name="month"
		                        pl="Month"
		                        />
			      		</div>
			      		<div className="col-sm-6 time">
		      				<label htmlFor="time">Start time</label>
		      				<ModalInput
		                        name="time"
		                        pl=""
		                        required
		                        />
		      				hh:mm
			      		</div>
			      	</div>

			      	<div className="row">
			      		<div className=" col-sm-6 location">
					      	<ModalInput
		                        name="address"
		                        pl="Address"
		                        required />
				      	</div>
				      	<div className="col-sm-6">
		                    <ModalInput
		                        name="city"
		                        pl="City"
	                        />
				      	</div>
			      	</div>

			      	<div className="row">
			      		<div className=" col-sm-6 location">
					      	<ModalInput
		                        name="state"
		                        pl="State"
		                        required />
				      	</div>
				      	<div className="col-sm-6">
		                    <ModalInput
		                        name="zipcode"
		                        pl="Zipcode"
	                        />
				      	</div>
			      	</div>

			      	<div className="description">
			      		<label><i className="icon-description"></i>Description</label>
			      		<ModalTextarea name="description" required/>
			      	</div>



			      	<div className="text-center">
			      		<button className="btn btn-primary">Create</button>
			      	</div>

			      	<div className="text-danger text-center">{notify}</div>

				</div>
			</Form>
		</div>
	</div>
    );
  }
});

export default  Modal;