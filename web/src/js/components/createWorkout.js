import React from 'react';
import { Form } from 'formsy-react';
import Actions from '../actions/actions';
import ModalInput from './modalInput';
import ModalTextarea from './modalTextarea';

const Modal = React.createClass({
    getInitialState() {
        return {
            notify: false
        };
    },
    submit(data) {
        Actions.createWorkout(data);
    },
    notifyFormError() {
        this.setState({notify: true});
    },
    hideModal() {
      Actions.hideModal();
    },
    render() {
        const notify = (this.state.notify) ? 'please fill all fields' : null;
        return(
          <div className="modal-content">
                <div className="modal-header">
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.hideModal}>
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 className="modal-title">Create New Session</h4>
                </div>
                <div className="modal-body">

                    <Form onValidSubmit={this.submit} onInvalidSubmit={this.notifyFormError}>
                      <div className="create-workout">
                          <div className="type"><ModalInput className="form-control" type="text" pl="Workout type" name="type" required/></div>
                          <div className="label"><ModalInput className="form-control" type="text" pl="Label" name="label" required/></div>

                          <div className="row">
                              <div className="col-sm-8 duration">
                                <label>Duration</label>
                                <div className="input-wrap"><ModalInput className="form-control" type="text" name="hours" pl="0" /><i className="change-quantity plus"></i><i className="change-quantity minus"></i></div> h.
                                <div className="input-wrap"><ModalInput className="form-control" type="text" name="minutes" pl="0"   required/><i className="change-quantity plus"></i><i className="change-quantity minus"></i></div> min.
                              </div>
                              <div className="col-sm-4 price">
                                <label htmlFor="price">Price</label>
                                <ModalInput className="form-control" type="text" name="price" required/>
                                <span className="currency">$</span>
                              </div>
                          </div>

                          <div className="description">
                              <label><i className="icon-description"></i>Description</label>
                              <ModalTextarea name="description" className="form-control" required/>
                          </div>

                          <div className="text-center">
                              <button className="btn btn-primary">Create</button>
                          </div>
                          <div className="text-danger text-center">{notify}</div>
                      </div>
                    </Form>
                </div>
              </div>
        );
    }
});

export default  Modal;