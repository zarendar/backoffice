import React from 'react';
import { Form } from 'formsy-react';
import Actions from '../actions/actions';
import ModalInput from './modalInput';
import ModalTextarea from './modalTextarea';

const createWorkoutLocation = React.createClass({
    getInitialState() {
        return {
            notify: false
        };
    },
    submit(data) {
        Actions.hideModal();
        Actions.newWorkoutLocation(data);
    },
    notifyFormError() {
        this.setState({notify: true});
    },
    render() {
        const notify = (this.state.notify) ? 'please fill all fields' : null;
        return( 
                <div className="modal-body">
                    <Form onValidSubmit={this.submit} onInvalidSubmit={this.notifyFormError}>
                      <div className="create-workout">
                          <label>Please select the location</label>
                          <div className="type">
                            <ModalInput
                              className="form-control"
                              type="select"
                              pl="List of existing locations"
                              name="location_id"
                              optionsArr = {this.props.location}/>
                          </div>

                          <label>Or add a new one</label>
                          <div className="row">
                              <div className="col-sm-4">
                              <div className="input-wrap"><ModalInput className="form-control" type="text" name="address" pl="address"/></div>
                              </div>
                              <div className="col-sm-3">
                              <div className="input-wrap"><ModalInput className="form-control" type="text" name="city" pl="city"/></div>
                              </div>
                              <div className="col-sm-2">
                              <div className="input-wrap"><ModalInput className="form-control" type="text" name="state" pl="state"/></div>
                              </div>
                              <div className="col-sm-3">
                              <div className="input-wrap"><ModalInput className="form-control" type="text" name="postcode" pl="postcode" /></div>
                              </div>
                          </div>
                          <div className="text-center">
                            <button className="btn btn-primary">Create and use</button>
                          </div>
                          <div className="text-danger text-center">{notify}</div>
                      </div>
                    </Form>
                </div>
        );
    }
});

export default  createWorkoutLocation;