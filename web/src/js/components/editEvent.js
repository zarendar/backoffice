import React from 'react';
import { Form } from 'formsy-react';
import Actions from '../actions/actions';
import ModalInput from './modalInput';
import ModalTextarea from './modalTextarea';


const Modal = React.createClass({
	getInitialState() {
	    return {
	        notify: false  
	    };
	},
	submit(data) {
	  data.id = this.props.data.id;
	  data.location_id = this.props.data.location_id;
	  Actions.editEvent(data);
	},
	notifyFormError() {
		this.setState({notify: true});
	},
  render() {
  	const notify = (this.state.notify) ? 'please fill all fields' : null;
  	const data = this.props.data;
  	const date = data.d_date.split('-');
  	let hours;
    let minutes;
    function parseMinutes(x) {hours=Math.floor(x/60); minutes=x% 60;}
    parseMinutes(data.duration);
    return(
		<div className="modal-body">
			<Form onValidSubmit={this.submit} onInvalidSubmit={this.notifyFormError}>
				<div className="create-new-event">
					<ModalInput
	                    name="ui_label"
	                    pl="Label"
	                    block="true"
	                    value={data.ui_label}
	                    required />

			      	<div className="row">
			      		<div className="col-sm-8 duration">
							<label>Duration</label>
							<ModalInput
								type="number"
		                        name="hours"
		                        pl="0"
		                        value={hours}
		                        required
		                        /> h.
							<ModalInput
								type="number"
		                        name="min"
		                        pl="0"
		                        value={minutes}
		                        required
		                        /> min.
			      		</div>
			      		<div className="col-sm-4 price">
							<label htmlFor="price">Price</label>
							<ModalInput
		                        name="price"
		                        pl=""
		                        value={data.price}
		                        required
		                        />
							<span className="currency">$</span>
			      		</div>
			      	</div>

			      	<div className="row">
			      		<div className="col-sm-6 date">
			      			<label>Date</label>
			      			<ModalInput
								type="number"
		                        name="day"
		                        pl="Day"
		                        value={date[2]}
		                        required
		                        />
		                    <ModalInput
								type="number"
		                        name="month"
		                        pl="Month"
		                        value={date[1]}
		                        required
		                        />
			      		</div>
			      		<div className="col-sm-6 time">
		      				<label htmlFor="time">Start time</label>
		      				<ModalInput
		                        name="time"
		                        pl=""
		                        value={data.start_time}
		                        required
		                        />
		      				hh:mm
			      		</div>
			      	</div>

			      	<div className="description">
			      		<label><i className="icon-description"></i>Description</label>
			      		<ModalTextarea name="description" value={data.description} required/>
			      	</div>

			      	

			      	<div className="text-center">
			      		<button className="btn btn-primary">Edit</button>
			      	</div>

			      	<div className="text-danger text-center">{notify}</div>

				</div>
			</Form>
	</div>
    );
  }
});

export default  Modal;