import React from 'react';
import { Form } from 'formsy-react';
import Actions from '../actions/actions';
import ModalInput from './modalInput';
import ModalTextarea from './modalTextarea';

const Modal = React.createClass({
    getInitialState() {
        return {
            notify: false
        };
    },
    submit(data) {
      data.id = this.props.data.id;
      data.location_id = this.props.data.location_id;
      Actions.editSession(data);
    },
    notifyFormError() {
        this.setState({notify: true});
    },
    render() {
        const notify = (this.state.notify) ? 'please fill all fields' : null;
        const item = this.props.data;
        let hours;
        let minutes;
        function parseMinutes(x) {hours=Math.floor(x/60); minutes=x% 60;}
        parseMinutes(item.duration);
        return(
                <div className="modal-body">
                    <Form onValidSubmit={this.submit} onInvalidSubmit={this.notifyFormError}>
                      <div className="create-workout">

                          <div className="type">
                            <ModalInput
                              className="form-control"
                              type="text"
                              pl="Workout type"
                              name="type"
                              value={item.workout_type_label}
                              required/>
                          </div>
                          <div className="label">
                            <ModalInput
                              className="form-control"
                              type="text"
                              pl="Label"
                              name="label"
                              value={item.ui_label}
                              required/>
                          </div>

                          <div className="row">
                              <div className="col-sm-8 duration">
                                <label>Duration</label>
                                <div className="input-wrap">
                                  <ModalInput
                                    className="form-control"
                                    type="text"
                                    name="hours"
                                    pl="0"
                                    value={hours}
                                    required/>
                                  <i className="change-quantity plus"></i><i className="change-quantity minus"></i>
                                </div> h.
                                <div className="input-wrap">
                                  <ModalInput
                                    className="form-control"
                                    type="text"
                                    name="minutes"
                                    pl="0"
                                    value={minutes}
                                    required/>
                                  <i className="change-quantity plus"></i><i className="change-quantity minus"></i>
                                </div> min.
                              </div>
                              <div className="col-sm-4 price">
                                <label htmlFor="price">Price</label>
                                <ModalInput className="form-control" type="text" name="price" value={item.price} required/>
                                <span className="currency">$</span>
                              </div>
                          </div>

                          <div className="description">
                              <label><i className="icon-description"></i>Description</label>
                              <ModalTextarea name="description" className="form-control" value={item.description} required/>
                          </div>

                          <div className="text-center">
                              <button className="btn btn-primary">Edit</button>
                          </div>
                          <div className="text-danger text-center">{notify}</div>
                      </div>
                    </Form>
                </div>
        );
    }
});

export default  Modal;