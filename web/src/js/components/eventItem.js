import React from 'react';
import Actions from '../actions/actions';

const EventItem = React.createClass({
    showModal() {
      Actions.showModal('editEvent', this.props.item);
    },
    render() {
      const item = this.props.item;
      return(
        <div className="event-item" onClick={this.showModal}>
          <div className="row">
            <div className="col-sm-4 name">{item.ui_label}</div>
            <div className="col-sm-2 price"><span>{`$ ${item.price}`}</span></div>
            <div className="col-sm-2 date">{item.d_date}</div>
            <div className="col-sm-2 time">{item.start_time}</div>
            <div className="col-sm-2 place">{item.address}</div>
          </div>
        </div>
      );
    }
});

export default EventItem;