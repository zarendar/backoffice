import React from 'react';
import ReactDOM from 'react-dom';
import Actions from '../actions/actions';
import {Url, ImgUrl} from '../util/constants';

const ImageUploader = React.createClass({
    getInitialState() {
      return {
        fileName: null
      }
    },
    change(e) {
      const _this = this
      const {value, width, height} = this.props;
      const reader = new FileReader();
      const file = e.target.files[0];
      const name = file['name'];
      // const fileType = file.type;
      // const fileSize = file.size;
      const maxFileSize = 4 * 1024 * 1000;
      const node = ReactDOM.findDOMNode(this.refs.desc);



      reader.onload = function(upload) {
        _this.setState({fileName: name});
        Actions.certificate(upload.target.result, $(node).val());
      }.bind(this)

      reader.readAsDataURL(file);   
    },
    render() {
      const {value, placeholder} = this.props;
      const id = Math.random();
      return(
        <div className="imageUploader">
           <div className="form-group file-wrapper">
              <input
                ref="desc"
                className="form-control" />
              <label className="input-icon" htmlFor={id}>
                <i className="icon-download"></i>
              </label>
              <input
                type="file"
                id={id}
                className="form-control"
                onChange={this.change} />
           </div>
           <div className="fileName">{this.state.fileName}</div>
        </div>
      );
    }
});


export default ImageUploader;