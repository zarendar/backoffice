import React from 'react';
import Logout from './logout';

const Header = React.createClass({
    render() {
      return(
        <div className="header">
          <div className="container-fluid">
            <div className="header-container">
              <div className="logo">
                <img src="src/img/logo.svg" alt=""/>
              </div>
              <div className="header-menu">
                <div className="header-menu-item">
                  <a href="http://map.sweatr.com/" target="_blank" className="search-btn" onClick={this.logout}>
                    <i className="icon-map"></i>
                  </a>
                </div>
                <Logout />
              </div>
            </div>
          </div>
        </div>
      );
    }
});

export default Header;