import React from 'react';
import Actions from '../actions/actions';
import {Url, ImgUrl} from '../util/constants';

const ImageUploader = React.createClass({
    getInitialState() {
        return {
             isNotValidFormat: false,
             isNotValidSize : false,
             isNotValidProportion: false
        };
    },
    change(e) {
      const _this = this
      const {value, width, height} = this.props;
      const reader = new FileReader();
      const file = e.target.files[0];
      const fileType = file.type;
      const fileSize = file.size;
      const maxFileSize = 4 * 1024 * 1000;
      var _URL = window.URL || window.webkitURL;
      let img;

      img = new Image();

      img.onload = function() {
          _this.setState({isNotValidFormat: false, isNotValidSize: false, isNotValidProportion: false})
          if (fileType != 'image/jpeg' && fileType != 'image/png' && fileType != 'image/jpg') {
            _this.setState({isNotValidFormat: true})
          } else if (fileSize > maxFileSize) {
            _this.setState({isNotValidSize: true, isNotValidFormat: false})
          } else if (!(+width <= this.width && this.width <= +width + 10) && !(+height <= this.height &&  this.height <= +height+ 10)) {
            _this.setState({isNotValidProportion: true, isNotValidSize: false, isNotValidFormat: false});
          } else {
            _this.setState({isNotValidProportion: false, isNotValidSize: false, isNotValidFormat: false})
            reader.readAsDataURL(file);
          }
      };
      img.onerror = function() {
        _this.setState({isNotValidProportion: false, isNotValidSize: false, isNotValidFormat: true})
      };
      img.src = _URL.createObjectURL(file);

      reader.onload = function(upload) {
        if (value == 'profile_cover') {
          _this.setState({ profileCover: upload.target.result });
          Actions.profileCover(upload.target.result);
        } else if (value == 'avatar') {
          _this.setState({ avatar: upload.target.result });
          Actions.avatar(upload.target.result);
        }
      }
    },
    render() {
      const {data, profileCover, avatar, isLoaded, successMessage} = this.props.settings;
      const {placeholder, value, width, height, type} = this.props;
      const isNotValidFormat = (this.state.isNotValidFormat) ? 'Is not valid format' : null;
      const isNotValidSize = (this.state.isNotValidsize) ? 'Is not valid size' : null;
      const isNotValidProportion = (this.state.isNotValidProportion) ? `Must be ${width}*${height} pixels` : null;
      let imgUrl;
      if (isLoaded) {
        if (value == 'profile_cover') {
          imgUrl = (profileCover)
            ?  profileCover
            : (data.cover_image_type) ? `${ImgUrl}/cover-images/${data.id}.${data.cover_image_type}` : null
        } else if (value == 'avatar') {
          imgUrl = (avatar)
            ?  avatar
            : (data.avatar_image_type) ? `${ImgUrl}/avatars/${data.id}.${data.avatar_image_type}` : null
        }
      }
      return(
        <div className="imageUploader">
           <div className="form-group file-wrapper">
              <label className="input-icon" htmlFor={value}>
                <i className="icon-download"></i>
              </label>
              <input
                type="file"
                id={value}
                className="form-control"
                onChange={this.change} />
              <div className="pl">{placeholder}</div>
           </div>
           <div className="cover-desc">({width}*{height} pixels .jpeg, .jpg or .png files only)</div>
           <div className="text-danger">{isNotValidFormat}{isNotValidSize}{isNotValidProportion}</div>
           <div className="img-view">
              <img
                className="img-responsive"
                src={imgUrl} alt=""/>
           </div>
              <img id="hidden-img" className="hidden" src="" alt=""/>
        </div>
      );
    }
});


export default ImageUploader;