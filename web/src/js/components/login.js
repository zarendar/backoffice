import React from "react";
import Actions from "../actions/actions";
import { Form } from 'formsy-react';
import SweatrInput from './sweatrInput';

require('../../styl/components/account.styl');

const Login = React.createClass({
  getInitialState() {
      return {
          signUp: false
      };
  },
  submit(data) {
    (this.state.signUp) ? Actions.signUp(data) : Actions.signIn(data);
  },
  toggleView() {
    (this.state.signUp) ? this.setState({signUp: false}) : this.setState({signUp: true});
  },
  render() {
    const errorMessage = this.props.errorMessage;
    return(
      <div className="account login">
        <Form onValidSubmit={this.submit}>
          <SweatrInput
            name="email"
            icon="email"
            pl="Public email address"
            validations="isEmail"
            validationError="This is not a valid email"
            required/>
          <SweatrInput
            type="password"
            name="pswd"
            icon="lock-filled"
            pl="Password"
            required/>
          {
            (this.state.signUp) ?
              (
               <div>
                 <SweatrInput
                    type="first_name"
                    name="first_name"
                    icon="user"
                    pl="Your First Name"
                    required/>
                  <SweatrInput
                    type="last_name"
                    name="last_name"
                    icon="user"
                    pl="Your Last Name"
                    required/>
                  <SweatrInput
                    type="phone"
                    name="phone"
                    icon="telephone"
                    pl="Your Telephone"
                    required/>
               </div>
              ) : null
          }
          <div className="row">
            <div className="col-xs-12">
              <button className="btn btn-primary btn-block">
                {(this.state.signUp) ? 'Sign Up' : 'Sign In'}
              </button>
              <span className="error"></span>
            </div>
          </div>


          <div className="login-footer" >
              <span onClick={this.toggleView}>
                {(this.state.signUp) ? 'Sign In' : 'Sign Up'}
              </span>
          </div>
          <div className="text-danger">{errorMessage}</div>
        </Form>

      </div>
    );
  }
});

export default  Login;