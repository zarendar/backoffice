import React from 'react';
import Reflux from 'reflux';
import Actions from '../actions/actions';

const Logout = React.createClass({
    logout() {
      Actions.logout();
    },
    render() {
      return(
        <div className="header-menu-item logout">
          <span className="search-btn" onClick={this.logout}>
            <i className="icon-logout"></i>
          </span>
        </div>
      );
    }
});

export default Logout;