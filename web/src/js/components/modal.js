import React from 'react';
import Actions from '../actions/actions';

const Modal = React.createClass({
  hideModal() {
    Actions.hideModal();
  },
  render() {
    return(
      <div className="modal" id="myModal" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div className="vertical-alignment-helper">
	          <div className="modal-dialog vertical-align-center" role="document">
                {this.props.children}
	          </div>
	        </div>
      </div>
    );
  }
});

export default  Modal;