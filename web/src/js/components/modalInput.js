import React from 'react';
import Formsy from 'formsy-react';

const SweatrInput = React.createClass({
  mixins: [Formsy.Mixin],
  changeValue(event) {
    this.setValue(event.currentTarget[this.props.type === 'checkbox' ? 'checked' : 'value']);
  },
  changeValueSelect(event) {
    this.setValue(event.target.value);
  },


  addLeadingZeros(n, length){
      let str = (n > 0 ? n : -n) + "";
      let zeros = "";
      for (let i = length - str.length; i > 0; i--)
          zeros += "0";
      zeros += str;
      return n >= 0 ? zeros : "-" + zeros;
  },

  onClickPlus(){
    if (this.getValue()){
      let number = parseInt(this.getValue());
      this.setValue(number + 1);
    } else {
      this.setValue(1);
    };
  },

  onClickMinus(){
    if (this.getValue()){
      let number = parseInt(this.getValue());
      if (number >= 1 ) {
        this.setValue(number - 1);
      };
    } else {
      this.setValue(0);
    };
  },

  render() {
    let {name, icon, pl} = this.props;
    let isFormSubmitted = this.isFormSubmitted();
    const notValid = isFormSubmitted && this.showError();
    const empty = isFormSubmitted && this.showRequired() && !this.getValue()
    let errorMessage = (notValid) ? this.getErrorMessage() : null;
    let isEmpty = (empty) ? 'This field is required' : null;
    const block = (this.props.block == 'true') ? 'block' : '';

    let optionNodes;

    if (this.props.type == 'select') {
      optionNodes = this.props.optionsArr.map((item, i) => {
        return <option key={i} value={item.id}>{item.address}</option>
      });
    }


    return(
      <div className={`input-wrap ${block}`}>
        {
          (this.props.type == 'textarea') ?
            (
              <textarea
                name="description"
                id={name}
                rows="6"
                className="form-control"
                onChange={this.changeValue}
                value={this.getValue()}>
              </textarea>
            ) :
            (this.props.type == 'number') ?  (
                <div>
                    <input
                    type={this.props.type || 'text'}
                    id={name}
                    className="form-control"
                    placeholder={pl}
                    onChange={this.changeValue}
                    value={this.getValue()} />
                  <i className="change-quantity plus" onClick={this.onClickPlus}></i>
                  <i className="change-quantity minus" onClick={this.onClickMinus}></i>
                </div>
              ) :
            (this.props.type == 'select') ?  (
                <div>
                    <select id={name} className="form-control" value={this.getValue()} onChange={this.changeValueSelect}>
                      {optionNodes}
                    </select>
                </div>
              )
            : (
              <input
                type={this.props.type || 'text'}
                id={name}
                className="form-control"
                placeholder={pl}
                onChange={this.changeValue}
                value={this.getValue()} />
            )
        }
      </div>

    );
  }
});

export default SweatrInput;