import React from 'react';
import Formsy from 'formsy-react';

const ModalTextarea = React.createClass({
  mixins: [Formsy.Mixin],
  changeValue(event) {
    this.setValue(event.currentTarget[this.props.type === 'checkbox' ? 'checked' : 'value']);
  },
  render() {
    let {name} = this.props;
    let isFormSubmitted = this.isFormSubmitted();
    const notValid = isFormSubmitted && this.showError();
    const empty = isFormSubmitted && this.showRequired() && !this.getValue()
    let errorMessage = (notValid) ? this.getErrorMessage() : null;
    let isEmpty = (empty) ? 'This field is required' : null;
    return(
      <div >
        <textarea
          name="description"
          id={name}
          rows="6"
          className="form-control"
          onChange={this.changeValue}
          value={this.getValue()}>
        </textarea>
      </div>

    );
  }
});

export default ModalTextarea;