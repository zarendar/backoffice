import React from 'react';
import ReactDOM from 'react-dom';
import Actions from '../actions/actions';

const SessionItem = React.createClass({
    editSession() {
      Actions.showModal('editSession', this.props.item);
    },
    componentDidMount(e) {
        const node = ReactDOM.findDOMNode(this.refs.fc);
        // store data so the calendar knows to render an event upon drop
        $(node).data('event', {
          title: $.trim($(this).text()), // use the element's text as the event title
          stick: true // maintain when user navigates (see docs on the renderEvent method)
        });

        // make the event draggable using jQuery UI
        $(node).draggable({
          zIndex: 999,
          revert: true,       // will cause the event to go back to its
          helper: "clone",     
          dragRevertDuration: 2000,
          revertDuration: 0, //  original position after the drag
          start: function() {
            Actions.dragSession(this.id);
          },
        }); 
    },

    render() {
      const item = this.props.item;
      return(
        <div id={item.id} className='fc-event' ref="fc" onClick={this.editSession}>
          <div className="title">{item.ui_label}</div>
          <div className="place">{item.address}</div>
          <div className="duration">{item.duration} min.</div>
          <button className="btn btn-primary">${item.price}</button>
        </div>
      );
    }
});

export default SessionItem;