import React from 'react';
import Formsy from 'formsy-react';

const SweatrInput = React.createClass({
  mixins: [Formsy.Mixin],
  changeValue(event) {
    this.setValue(event.currentTarget[this.props.type === 'checkbox' ? 'checked' : 'value']);
  },
  render() {
    let {name, icon, pl} = this.props;
    let isFormSubmitted = this.isFormSubmitted();
    const notValid = isFormSubmitted && this.showError();
    const empty = isFormSubmitted && this.showRequired() && !this.getValue()
    let errorMessage = (notValid) ? this.getErrorMessage() : null;
    let isEmpty = (empty) ? 'This field is required' : null;
    return(
      <div>
        {
          (this.props.type == 'textarea') ?
            (
              <textarea
                name="description"
                id={name}
                rows="6"
                className="form-control"
                onChange={this.changeValue}
                value={this.getValue()}>
              </textarea>
            ) :
            (
              <div className="form-group">
                <label className="input-icon" htmlFor={name}>
                  <i className={`icon-${icon}`}></i>
                </label>
                <input
                  type={this.props.type || 'text'}
                  id={name}
                  className="form-control"
                  placeholder={pl}
                  onChange={this.changeValue}
                  value={this.getValue()}
                  checked={this.props.type === 'checkbox' && this.getValue() ? 'checked' : null} />
               </div>
            )
        }
        <p className="text-danger">{errorMessage} {isEmpty}</p>
      </div>

    );
  }
});

export default SweatrInput;