'use strict';

import React from 'react';
import { render } from 'react-dom';
import Routes from './routes';

require('fullcalendar/dist/fullcalendar.css')
require('../styl/app.styl');
require('../styl/header.styl');
require('../styl/general.styl');
require('../styl/components/account.styl');
require('../styl/components/sessions.styl')

require('bootstrap-styl/js/modal.js');

if (module.hot) module.hot.accept();

render(Routes, document.getElementById('app'));