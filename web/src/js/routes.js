'use strict'

import React from 'react';
import { Router, Route, IndexRoute, Link, browserHistory, Redirect  } from 'react-router';

import $ from 'jquery';
import _ from 'lodash';

window.jQuery = $;
window.$ = $;
window._ = _;

require('fullcalendar/dist/fullcalendar.css')
require('../styl/app.styl');
require('../styl/components/account.styl');
require('../styl/header.styl');
require('../styl/general.styl');

require('../styl/components/sessions.styl')

require('bootstrap-styl/js/modal.js');

const App = require('./app');
import Account from './views/account';
import Settings from './views/settings';
import PersonalData from './views/settings/personalData';
import SocialNetworks from './views/settings/socialNetworks';
import SubSettings from './views/settings/settings';
import Certificates from './views/settings/certificates';
import Images from './views/settings/images';
import Workouts from './views/workouts';
import Events from './views/events';

export default (
  <Router history={browserHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={Account} />
      <Route path="account" component={Account} >
        <IndexRoute component={Workouts} />
        <Route path="settings" component={Settings} >
          <IndexRoute component={PersonalData} />
          <Route path="socialNetworks" component={SocialNetworks} />
          <Route path="settings" component={SubSettings} />
          <Route path="certificates" component={Certificates} />
          <Route path="images" component={Images} />
        </Route>
        <Route path="workouts"  component={Workouts} />
        <Route path="events"  component={Events} />
      </Route>
    </Route>
  </Router>
);