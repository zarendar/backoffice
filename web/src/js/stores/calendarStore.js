'use strict';

import Reflux from 'reflux';
import Moment from 'moment';
import Actions from '../actions/actions';
import {Url} from '../util/constants';

let calendarState = {
    isLoaded: false,
    timeEvents: [],
    data: [],
    lastEvent: {},
    locations: [],
    addedWorkouts: [],
};

const CalendarStore = Reflux.createStore({

    listenables: Actions,

    eventAction(start_time, end_time, selected_day, id) {

        var counter = 0;
        calendarState.timeEvents.forEach(function(element){
            if(element.id == id){
                element.start_time = start_time;
                element.end_time = end_time;
                element.week_day = selected_day;
            } else {
                counter++;
            }
        });

        if(counter == calendarState.timeEvents.length) {
           calendarState.timeEvents.push({"start_time": start_time,"end_time": end_time, "week_day": selected_day, "id": id});
        }
        calendarState.lastEvent = {'week_day': selected_day, 'start_time': start_time, 'session_id': id};
        this.trigger(calendarState);
    },

    removeAction(id) {
        calendarState.timeEvents.forEach(function(element){
            if(element.id == id){
                var position = calendarState.timeEvents.indexOf(element);
                calendarState.timeEvents.splice(position, 1);
            }
        });
        this.trigger(calendarState);
    },

    createWorkout(form_data){

        const _success = (resp) => {
            calendarState.data.push(resp.data);
            this.trigger(calendarState);
            Actions.hideModal();
        };
        const _error = (resp) => {
            debugger
        };

        const url = `${Url}/provider/session`;

        form_data.hours = (form_data.hours ) ? form_data.hours : 0;

        //post query here

        $.ajax({
            method: 'POST',
            url: url,
            xhrFields: {
                withCredentials: true
            },
            data: {
                data: {
                    workout_type_label: form_data.type,
                    ui_label: form_data.label,
                    duration: parseInt(form_data.hours) * 60 + parseInt(form_data.minutes),
                    price: form_data.price,
                    description: form_data.description,
                }
            },
            success(resp) {
                _success(resp)
            },
            error(resp) {
                _error(resp)
            }
        });
    },

    editSession(data){
        const _this = this;
        const {label, hours, minutes, description, price, type, id, provider_id} = data;
        const _success = (resp) => {
             const newArr = calendarState.data.map((item) => {
                if (item.id == id) {
                  item.ui_label = resp.data.ui_label;
                  item.duration = resp.data.duration;
                  item.price = resp.data.price;
                  item.description = resp.data.description;
                  item.workout_type_label = resp.data.workout_type_label;
                }
                return item;
              });

              calendarState.data = newArr;
            _this.trigger(calendarState);
            Actions.hideModal();
        };
        const _error = (resp) => {
            debugger
        };

        const url = `${Url}/provider/session/${id}`;

         data.hours = (data.hours ) ? data.hours : 0;

        //post query here

        $.ajax({
            method: 'POST',
            url: url,
            xhrFields: {
                withCredentials: true
            },
            data: {
                data: {
                    workout_type_label: type,
                    ui_label: label,
                    duration: parseInt(hours) * 60 + parseInt(minutes),
                    price: price,
                    description: description,
                    provider_id: provider_id
                }
            },
            success(resp) {
                _success(resp)
            },
            error(resp) {
                _error(resp)
            }
        });
    },

    initSessions(value) {
        const _this = this;
        const url = `${Url}/provider/session`;
        const url_locations = `${Url}/provider/location`;
        const _success = (resp) => {
            calendarState.data = resp.data;
            _this.trigger(calendarState);
            calendarState.isLoaded = true;
            Actions.initWorkouts();
          };
        const _error = (resp) => {
            debugger
        };
        const _success_locations = (resp) => {
            calendarState.locations = resp.data;
            _this.trigger(calendarState);
          };
        const _error_locations = (resp) => {
            debugger
        };
        $.ajax({
            method: 'GET',
            url: url,
            xhrFields: {
                withCredentials: true
            },
            success(resp) { _success(resp) },
            error(resp) { _error(resp) }
        });
        $.ajax({
            method: 'GET',
            url: url_locations,
            xhrFields: {
                withCredentials: true
            },
            success(resp) { _success_locations(resp) },
            error(resp) { _error_locations(resp) }
        });
    },
    initWorkouts() {
        const _this = this;
        const url_workouts = `${Url}/provider/workout`;
        const _success_workouts = (resp) => {
            if (!resp.data.length) {
              calendarState.addedWorkouts = "empty";
            } else {
              calendarState.addedWorkouts = resp.data;
            }

            _this.trigger(calendarState);
          };
        const _error_workouts = (resp) => {
            debugger
        };
        $.ajax({
            method: 'GET',
            url: url_workouts,
            xhrFields: {
                withCredentials: true
            },
            success(resp) { _success_workouts(resp) },
            error(resp) { _error_workouts(resp) }
        });
    },
    changeWorkout(session_id, week_day, start_time, location_id, workout_id){
        const time = Moment(start_time, 'h m a').format('H:mm');
        const _success = (resp) => {
        };
        const _error = (resp) => {
        };
        const url = `${Url}/provider/workout/`+workout_id;

        //post query here

        $.ajax({
            method: 'POST',
            url: url,
            xhrFields: {
                withCredentials: true
            },
            data: {
                data: {
                    session_id: session_id,
                    week_day: week_day,
                    start_time: time,
                    location_id: location_id,
                },
            },
            success(resp) {
                _success(resp)
            },
            error(resp) {
                _error(resp)
            }
        });

    },
    newWorkoutLocation(form_data){
        const time = Moment(calendarState.lastEvent.start_time, 'h m a').format('H:mm');
        const _success = (resp) => {

        };
        const _error = (resp) => {
            debugger
        };
        const url = `${Url}/provider/workout`;

        //post query here

        $.ajax({
            method: 'POST',
            url: url,
            xhrFields: {
                withCredentials: true
            },
            data: {
                data: {
                    session_id: calendarState.session_id,
                    week_day: calendarState.lastEvent.week_day,
                    start_time: time,
                    location_id: form_data.location_id,
                },
                location: {
                    address: form_data.address,
                    city: form_data.city,
                    state: form_data.state,
                    postcode: form_data.postcode,
                }
            },
            success(resp) {
                _success(resp)
            },
            error(resp) {
                _error(resp)
            }
        });

    },
    dragSession(id) {
        calendarState.session_id = id;
        this.trigger(calendarState);
    },
    getDefaultData() {
        return calendarState;
    }

});

export default CalendarStore;