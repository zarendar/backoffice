'use strict';

import Reflux from 'reflux';
import Actions from '../actions/actions';

import {Url} from '../util/constants';

let eventsState = {
    isLoaded: false,
    data: []
};

const EventsStore = Reflux.createStore({

  listenables: Actions,

  initEvents(value) {
    const _this = this;
    const url = `${Url}/provider/event`;
    const _success = (resp) => {
        eventsState.data = resp.data;
        eventsState.isLoaded = true;
        _this.trigger(eventsState);
      };
    const _error = (resp) => {
      debugger
    };
      $.ajax({
        method: 'GET',
        url: url,
        xhrFields: {
            withCredentials: true
        },
        success(resp) { _success(resp) },
        error(resp) { _error(resp) }
      });
  },

  createEvent(data) {
    const {ui_label, description, hours, min, month, day, price, time, address, city, state, zipcode} = data;
    const _this = this;
    const url = `${Url}/provider/event`;
    const durtion = hours * 60 + min;
    const d_date = `2016-${month}-${day}`;
    const start_time = `${time}:00`;
    const _success = (resp) => {
        eventsState.data.push(resp.data);
        _this.trigger(eventsState);
        Actions.hideModal();
      };
    const _error = (resp) => {
      debugger
      // sessionState.isLoaded = true;
      // _this.trigger(sessionState);
    };
    data.hours = (data.hours ) ? data.hours : 0;
    // sessionState.isInit = value;
      $.ajax({
        method: 'POST',
        url: url,
        xhrFields: {
            withCredentials: true
        },
        data: {
          data: {
            ui_label: ui_label,
            duration: durtion,
            price: price,
            description: description,
            d_date: d_date,
            start_time: start_time
          },
          location: {
            address: address,
            city: city,
            state: state,
            postcode: zipcode
          }
        },
        success(resp) { _success(resp) },
        error(resp) { _error(resp) }
      });

  },

  editEvent(data) {
    const {ui_label, description, hours, min, month, day, price, time, address, city, state, zipcode, id, location_id} = data;
    const _this = this;
    const url = `${Url}/provider/event/${id}`;
    const durtion = hours * 60 + min;
    const d_date = `2016-${month}-${day}`;
    const start_time = `${time}:00`;
    const _success = (resp) => {
        // eventsState.data.push(resp.data);
          const newArr = eventsState.data.map((item) => {
            if (item.id == id) {
              item.ui_label = resp.data.ui_label;
              item.duration = resp.data.duration;
              item.price = resp.data.price;
              item.description = resp.data.description;
              item.d_date = resp.data.d_date;
              item.start_time = resp.data.start_time;
            }
            return item;
          });

          eventsState.data = newArr;
        _this.trigger(eventsState);
        Actions.hideModal();
      };
    const _error = (resp) => {
      debugger
      // sessionState.isLoaded = true;
      // _this.trigger(sessionState);
    };
    data.hours = (data.hours ) ? data.hours : 0;
    // sessionState.isInit = value;
      $.ajax({
        method: 'POST',
        url: url,
        xhrFields: {
            withCredentials: true
        },
        data: {
          data: {
            ui_label: ui_label,
            duration: durtion,
            price: price,
            description: description,
            d_date: d_date,
            start_time: start_time,
            location_id: location_id
          }
        },
        success(resp) { _success(resp) },
        error(resp) { _error(resp) }
      });

  },

  getDefaultData() {
      return eventsState;
  }

});

export default EventsStore;