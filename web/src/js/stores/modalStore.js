'use strict';

import Reflux from 'reflux';
import Actions from '../actions/actions';

import {Url} from '../util/constants';

let modalState = {
    type: false,
    data: {}
};

const ModalStore = Reflux.createStore({

  listenables: Actions,

  showModal(type, data, location) {
    modalState.type = type;
    modalState.data = data;
    modalState.location = location;
    this.trigger(modalState);
  },

  hideModal() {
    $('#myModal').modal('hide');
    modalState.type = false;
    modalState.data = []
    this.trigger(modalState);
  },

  getDefaultData() {
      return modalState;
  }

});

export default ModalStore;