'use strict';

import Reflux from 'reflux';
import md5 from 'blueimp-md5';
import Actions from '../actions/actions';

import {Url} from '../util/constants';

let sessionState = {
    isInit: false,
    isSigned: false,
    isLoaded: false,
    errorMessage: false
};

const SessionStore = Reflux.createStore({

  listenables: Actions,

  init(value) {
    const _this = this;
    const url = `${Url}/account/init`;
    const _success = (resp) => {
      if (resp.result)
        sessionState.isSigned = true;
        sessionState.isLoaded = true;
        _this.trigger(sessionState);
      };
    const _error = (resp) => {
      sessionState.isLoaded = true;
      _this.trigger(sessionState);
    };
    sessionState.isInit = value;
    if (value) {
      $.ajax({
        method: 'GET',
        url: url,
        xhrFields: {
            withCredentials: true
        },
        success(resp) { _success(resp) },
        error(resp) { _error(resp) }
      });
    }

  },

  signIn(data) {
    const _this = this;
    const email = data.email;
    const pass = md5(data.pswd);
    const url = `${Url}/account/sign_in/${email}/${pass}`;
    const _success = (resp) => {
      if (resp.result)
      sessionState.isSigned = true;
      sessionState.errorMessage = false;
      _this.trigger(sessionState);
      window.location.hash = 'account/workouts';
    };
    const _error = (resp) => {
      sessionState.errorMessage = resp.responseJSON.message;
      _this.trigger(sessionState);
    };
    $.ajax({
      method: 'GET',
      url: url,
      xhrFields: {
        withCredentials: true
      },
      success(resp) { _success(resp) },
      error(resp) { _error(resp) }
    });
  },

  signUp(data) {
    const _this = this;
    const url = `${Url}/account/sign_up/provider`;
    const _success = (resp) => {
      sessionState.isSigned = true;
      _this.trigger(sessionState);
    };
    const _error = (resp) => {
      debugger
    };
    data.pswd= md5(data.pswd);
    $.ajax({
      method: 'POST',
      url: url,
      xhrFields: {
        withCredentials: true
      },
      data: {
        data: data
      },
      success(resp) { _success(resp) },
      error(resp) { _error(resp) }
    });
  },

  logout() {
    const _this = this;
    const url = `${Url}/account/sign_out`;
    const _success = (resp) => {
      console.log(resp);
      sessionState.isSigned = false;
      localStorage.setItem('isSigned', '');
      _this.trigger(sessionState);
      window.location.hash = '';
    };
    const _error = (resp) => {
      console.log(resp);
    };
    $.ajax({
      method: 'GET',
      url: url,
      xhrFields: {
        withCredentials: true
      },
      success(resp) { _success(resp) },
      error(resp) { _error(resp) }
    });
  },

  getDefaultData() {
      return sessionState;
  }

});

export default SessionStore;