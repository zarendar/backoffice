'use strict';

import Reflux from 'reflux';
// import md5 from 'blueimp-md5';
import Actions from '../actions/actions';
import {Url, ImgUrl} from '../util/constants';

let settingsState = {
  isLoaded: false,
  data: [],
  profileCover: null,
  avatar: null,
  certificate: [],
  certificates: [],
  successMessage: {
    personalData: false,
    img: false,
    socialNetworks: false,
    settings: false
  }
};

const SettingsStore = Reflux.createStore({

  listenables: Actions,

  initSettings(value) {
    const _this = this;
    const settingsData = settingsState.data;
    const url = `${Url}/provider/settings`;
    const _success = (resp) => {
      const data = resp.data;
      settingsState.data = data;
      settingsState.isLoaded = true;
      _this.trigger(settingsState);
    };
    const _error = (resp) => {
      debugger
    };
    $.ajax({
      method: 'GET',
      url: url,
      xhrFields: {
        withCredentials: true
      },
      success(resp) { _success(resp) },
      error(resp) { _error(resp) }
    });
  },

  initCertificates(value) {
    const _this = this;
    const settingsData = settingsState.data;
    const url = `${Url}/provider/certificates`;
    const _success = (resp) => {
      const data = resp.data;
      settingsState.certificates = data;
      // settingsState.isLoadedCertificates = true;
      _this.trigger(settingsState);
    };
    const _error = (resp) => {
      debugger
    };
    $.ajax({
      method: 'GET',
      url: url,
      xhrFields: {
        withCredentials: true
      },
      success(resp) { _success(resp) },
      error(resp) { _error(resp) }
    });
  },

  submitSettings(requestData, type) {
    const _this = this;
    const url = `${Url}/provider/settings`;
    const _success = (resp) => {
      if (type == 'socialNetworks') {
        settingsState.data.facebook_url = requestData.facebook_url;
        settingsState.data.twitter_url = requestData.twitter_url;
        settingsState.data.instagram_url = requestData.instagram_url;
        settingsState.successMessage.socialNetworks = true;
      } else {
        settingsState.data.mbo_studio_id = requestData.mbo_studio_id;
        settingsState.successMessage.settings = true;
      }
      Actions.removeText();
      _this.trigger(settingsState);
    };
    const _error = (resp) => {
      debugger
    };
    $.ajax({
      method: 'POST',
      url: url,
      xhrFields: {
        withCredentials: true
      },
      data: {
        data: requestData,
      },
      success(resp) { _success(resp) },
      error(resp) { _error(resp) }
    });
  },
  submitPersonalData(requestData) {
    const _this = this;
    const url = `${Url}/provider/personal_data`;
    const _success = (resp) => {
      settingsState.data.name = requestData.name;
      settingsState.data.public_email = requestData.public_email;
      settingsState.data.phone = requestData.phone;
      settingsState.data.billing_address = requestData.billing_address;
      settingsState.data.description = requestData.description;
      settingsState.successMessage.personalData = true;
      Actions.removeText();
      _this.trigger(settingsState);
    };
    const _error = (resp) => {
      debugger
    };
    $.ajax({
      method: 'POST',
      url: url,
      xhrFields: {
        withCredentials: true
      },
      data: {
        data: requestData,
      },
      success(resp) { _success(resp) },
      error(resp) { _error(resp) }
    });
  },

  submitSettingsImg(profileCover, avatar) {
    const _this = this;
    const url = `${Url}/provider/images`;
    const _success = (resp) => {
       settingsState.successMessage.img = true;
       Actions.removeText();
      _this.trigger(settingsState);
    };
    const _error = (resp) => {
      debugger
    };
    if (profileCover) {
      $.ajax({
        method: 'POST',
        url: url,
        xhrFields: {
          withCredentials: true
        },
        data: {
          data: {
            profile_cover: profileCover,
          }
        },
        success(resp) { _success(resp) },
        error(resp) { _error(resp) }
      });
    } else if (avatar) {
      $.ajax({
        method: 'POST',
        url: url,
        xhrFields: {
          withCredentials: true
        },
        data: {
          data: {
            avatar: avatar,
          }
        },
        success(resp) { _success(resp) },
        error(resp) { _error(resp) }
      });
    }
  },

  submitSettingsCertificate(certificate) {
    const _this = this;
    const url = `${Url}/provider/certificates`;
    const _success = (resp) => {
      const data = resp.data;
       settingsState.successMessage.crtificate = true;
       settingsState.certificates = settingsState.certificates.concat(settingsState.certificate);
      _this.trigger(settingsState);
    };
    const _error = (resp) => {
      debugger
    };
    $.ajax({
      method: 'POST',
      url: url,
      xhrFields: {
        withCredentials: true
      },
      data: {
        data: settingsState.certificate,
      },
      success(resp) { _success(resp) },
        error(resp) { _error(resp) }
      });
  },

  profileCover(profileCover) {
    settingsState.profileCover = profileCover;
    this.trigger(settingsState);
  },

  avatar(avatar) {
    settingsState.avatar = avatar;
    this.trigger(settingsState);
  },

  certificate(certificate, desc) {
    settingsState.certificate.push({file: certificate, description: desc});
    this.trigger(settingsState);
  },

  removeText() {
    const _this = this;
    setTimeout(function() {
      settingsState.successMessage.personalData = false;
      settingsState.successMessage.socialNetworks = false;
      settingsState.successMessage.settings = false;
      settingsState.successMessage.img = false;
      _this.trigger(settingsState);
    }, 3000)
  },

  getDefaultData() {
      return settingsState;
  }

});

export default SettingsStore;