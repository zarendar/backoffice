'use strict';

import Reflux from 'reflux';
import Actions from '../actions/actions';

let testData = {
  data: null
}

const TestStore = Reflux.createStore({
  listenables: Actions,
  test() {
    testData.data = 1;
    this.trigger(testData);
  },
  getDefaultData() {
    return testData;
  }

});

export default TestStore;