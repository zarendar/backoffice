import React from 'react';
import AccountNavigation from '../components/accountNavigation';

const Account = React.createClass({
    render() {
      return(
        <div className="account">
          <AccountNavigation />
          <div className="account-content">
            {this.props.children}
          </div>
      </div>
      );
    }
});

export default Account;