import React from 'react';
import Reflux from 'reflux';
import Actions from '../actions/actions';
import EventsStore from '../stores/eventsStore';
import EventItem from '../components/eventItem';

const DropIns = React.createClass({
	mixins: [
      Reflux.listenTo(EventsStore, 'onEventsUpdate'),
    ],
    getInitialState() {
      return {
        events: EventsStore.getDefaultData(),
      };
    },
    onEventsUpdate(newEventsState) {
      this.setState({ events: newEventsState });
    },
	componentWillMount() {
		Actions.initEvents(true);
	},
	showModal() {
		Actions.showModal('createEvent');
	},
    render() {
      const isLoaded = this.state.events.isLoaded;
      let eventsNodes;
      if (isLoaded) {
        eventsNodes = this.state.events.data.map((item, i) => {
      	 return <EventItem item={item} key={i} />
        });
      } else {
        eventsNodes = (
          <div className="loader">
            <img src="src/img/preloader.gif" alt=""/>
          </div>
        )
      }
      return(
        <div className="events">
          <div className="container">
				<div className="add-event" onClick={this.showModal}>+
				</div>
	          	<div className="events-list">
	          		{eventsNodes}
	          	</div>
          </div>
        </div>
      );
    }
});


export default DropIns;