import React from 'react';
import Reflux from 'reflux';
import Actions from '../actions/actions';
import SettingsStore from '../stores/settingsStore';
import AccountTabMenu from '../components/accountTabMenu';



const Settings = React.createClass({
    mixins: [
      Reflux.listenTo(SettingsStore, 'onSettingsUpdate'),
    ],
    getInitialState() {
      return {
        settings: SettingsStore.getDefaultData(),
      };
    },
    onSettingsUpdate(newSettingsState) {
      this.setState({ settings: newSettingsState });
    },
    componentWillMount() {
      Actions.initSettings(true);
    },
    render() {
      const {data, isLoaded, profileCover, avatar, certificate, certificates, successMessage} = this.state.settings;
      return(
        <div className="settings">
          <AccountTabMenu />
          <div className="tab-content">
            {React.cloneElement(this.props.children,
              {
                data: data,
                isLoaded: isLoaded,
                profileCover: profileCover,
                avatar: avatar,
                certificate: certificate,
                certificates: certificates,
                successMessage: successMessage
              })}
          </div>
        </div>
      );
    }
});

export default Settings;