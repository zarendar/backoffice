import React from 'react';
import Actions from '../../actions/actions';
import FileUploader from '../../components/fileUploader';

const Certificates = React.createClass({
  getInitialState() {
      return {
          inputs: [
            {type: 'input'},
          ]
      };
  },
  addInput() {
    var a = this.state.inputs;
    a.push({type: 'input'});
    this.setState({inputs: a});
  },
    submit(e) {
      e.preventDefault();
      Actions.submitSettingsCertificate(this.props.certificate);
    },
    componentWillMount() {
      Actions.initCertificates(true);
    },
    render() {
      const successProfileCover = (this.props.successMessage.img) ? 'Updated' : null;


      const certificateNodes = this.props.certificates.map((item, i) => {
        const url = `https://s3-ap-southeast-2.amazonaws.com/sweatr-web/providers/certificates/${item.id}.${item.file_type}`;
        return (
          <li className="certif-item" key={i}>
            <span>{item.description}</span>
            <a href={url} target="_blank" className="pull-right"><i className="icon-download"></i></a>
          </li>
        )
      })
      const inputNodes = this.state.inputs.map((item, i) => {
        return (
          <FileUploader
            key={i}
            placeholder="Your certificate"
            value = "file" />
        );
      })
      const title = (this.props.certificates.length)
        ? 'Previously uploaded Certificates:'
        : 'Certificates not upload yet'
      return(
        <div className="settings">
          <div className="container">
            <div className="row">
              <div className="col-xs-6 col-xs-offset-3">
                 <div className="account-content">
                    <form onSubmit={this.submit}>
                      {inputNodes}

                      <div className="text-right btns">
                        <div onClick={this.addInput} className="btn btn-default btn-more">add more input</div>
                        <button type="submit" className="btn btn-primary">
                          Submit
                        </button>
                        <div className="text-success">{successProfileCover}</div>
                      </div>
                    </form>

                    <hr/>
                    <h4>{title}</h4>
                    <ul className="list-unstyled">
                      {certificateNodes}
                    </ul>
                 </div>
              </div>
            </div>
           </div>
        </div>
      );
    }
});


export default Certificates;