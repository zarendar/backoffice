import React from 'react';
import Actions from '../../actions/actions';
import ImageUploader from '../../components/imageUploader';

const Images = React.createClass({
    submit(e) {
      e.preventDefault();
      Actions.submitSettingsImg(this.props.profileCover, this.props.avatar);
    },
    render() {
      const successProfileCover = (this.props.successMessage.img) ? 'Updated' : null;
      return(
        <div className="settings">
          <div className="container">
            <div className="row">
              <div className="col-xs-6 col-xs-offset-3">
                 <div className="account-content">
                    <form onSubmit={this.submit}>
                      <ImageUploader
                        placeholder="Your profile avatar"
                        value = "avatar"
                        width = "150"
                        height = "150"
                        settings = {this.props} />
                      <ImageUploader
                        placeholder="Your profile cover"
                        value = "profile_cover"
                        width = "970"
                        height = "180"
                        settings = {this.props} />
                      <div className="text-right">
                        <button type="submit" className="btn btn-primary">
                          Submit
                        </button>
                      </div>
                      <div className="text-success">{successProfileCover}</div>
                    </form>
                 </div>
              </div>
            </div>
           </div>
        </div>
      );
    }
});


export default Images;