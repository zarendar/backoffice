import React from 'react';
import Actions from '../../actions/actions';
import { Form } from 'formsy-react';
import SweatrInput from '../../components/sweatrInput';

const PersonalData = React.createClass({
    submit(data) {
      Actions.submitPersonalData(data);
    },
    render() {
      const {data, isLoaded, successMessage} = this.props;
      const successText = (successMessage.personalData) ? 'Updated' : null;

      let personalData;
      if (isLoaded) {
        personalData = (
          <div className="container">
            <div className="row">
              <div className="col-xs-6 col-xs-offset-3">
                 <div className="account-content">
                    <Form onValidSubmit={this.submit}
                      ref="form">
                     <SweatrInput
                        name="name"
                        icon="user"
                        value={data.name}
                        pl="Label"
                        validations="isExisty"
                        required/>
                      <SweatrInput
                        name="public_email"
                        icon="email"
                        value={data.public_email}
                        pl="Public email adress"
                        validations="isEmail"
                        validationError="This is not a valid email"
                        required/>
                      <SweatrInput
                        name="phone"
                        icon="telephone"
                        value={data.phone}
                        pl="Phone number"
                        validations="isNumeric"
                        validationError="This is not a valid telephone"
                        required/>
                      <SweatrInput
                        name="billing_address"
                        icon="adress"
                        value={data.billing_address}
                        pl="Address (billing)"
                        required/>
                      <div className="description-layer">
                        <label htmlFor="description">
                          <i className="icon-description"></i>
                          <span>Description</span>
                        </label>
                        <SweatrInput
                          type="textarea"
                          name="description"
                          value={data.description}
                          icon="description"
                          required/>
                      </div>
                      <div className="text-right">
                        <button type="submit" className="btn btn-primary">
                          Submit
                        </button>
                      </div>
                      <div className="text-success">{successText}</div>
                    </Form>
                 </div>
              </div>
            </div>
           </div>
        )
      } else {
        personalData = (
          <div className="loader">
            <img src="src/img/preloader.gif" alt=""/>
          </div>
        )
      }
      return(
        <div className="personal-data">
          {personalData}
        </div>
      );
    }
});


export default PersonalData;