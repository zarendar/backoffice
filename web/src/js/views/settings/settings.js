import React from 'react';
import Actions from '../../actions/actions';
import { Form } from 'formsy-react';
import SweatrInput from '../../components/sweatrInput';

const Settings = React.createClass({
    submit(data) {
      Actions.submitSettings(data);
    },
    render() {
      const {data, isLoaded, successMessage} = this.props;
      const pl = (data.mbo_studio_id == 0 ) ? null : data.mbo_studio_id;
      const successText = (successMessage.settings) ? 'Updated' : null;
      let settings ;
      if (isLoaded) {
        settings = (
            <div className="container">
              <div className="row">
                <div className="col-xs-6 col-xs-offset-3">
                   <div className="account-content">
                      <Form onValidSubmit={this.submit} >
                        <SweatrInput
                        	name="mbo_studio_id"
                        	icon="facebook"
                        	pl={"Mind Body Online StudioID (if applicable)"}
                        	value={pl}/>
                        <div className="text-right">
                          <button type="submit" className="btn btn-primary">
                            Submit
                          </button>
                        </div>
                         <div className="text-success">{successText}</div>
                      </Form>
                   </div>
                </div>
              </div>
             </div>
        )
    } else {
        settings = (
            <div className="loader">
              <img src="src/img/preloader.gif" alt=""/>
            </div>
          )
    }
    return(
      <div className="settings">
        {settings}
      </div>
    );
  }
});


export default Settings;