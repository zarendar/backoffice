import React from 'react';
import Actions from '../../actions/actions';
import { Form } from 'formsy-react';
import SweatrInput from '../../components/sweatrInput';

const SocialNetworks = React.createClass({
    submit(data) {
      Actions.submitSettings(data, 'socialNetworks');
    },
    render() {
      const {data, isLoaded, successMessage} = this.props;
      const successText = (successMessage.socialNetworks) ? 'Updated' : null;
      let socialNetworks ;
      if (isLoaded) {
        socialNetworks = (
            <div className="container">
              <div className="row">
                <div className="col-xs-6 col-xs-offset-3">
                   <div className="account-content">
                      <Form onValidSubmit={this.submit} >
                        <SweatrInput name="facebook_url" icon="facebook" pl="Facebook" value={data.facebook_url}/>
                        <SweatrInput name="twitter_url" icon="twitter" pl="Twitter" value={data.twitter_url} />
                        <SweatrInput name="instagram_url" icon="instagram" pl="Instagram" value={data.instagram_url}/>
                        <div className="text-right">
                          <button type="submit" className="btn btn-primary">
                            Submit
                          </button>
                        </div>
                        <div className="text-success">{successText}</div>
                      </Form>
                   </div>
                </div>
              </div>
             </div>
        )
    } else {
        socialNetworks = (
            <div className="loader">
              <img src="src/img/preloader.gif" alt=""/>
            </div>
          )
    }
    return(
      <div className="social-networks">
        {socialNetworks}
      </div>
    );
  }
});


export default SocialNetworks;