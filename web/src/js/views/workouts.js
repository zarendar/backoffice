import React from 'react';
import Reflux from 'reflux';


import CreateWorkout from '../components/createWorkout.js';
import CreateWorkoutLocation from '../components/createWorkoutLocation.js';
import CalendarStore from '../stores/calendarStore';

import Actions from '../actions/actions';

import SessionItem from '../components/sessionItem';
import Calendar from '../components/calendar';


const Sessions = React.createClass({
  mixins: [
    Reflux.listenTo(CalendarStore, 'onCalendarUpdate'),
  ],
  getInitialState() {
    return {
      workouts: CalendarStore.getDefaultData(),
    };
  },
  onCalendarUpdate(newCalendarState) {
      this.setState({ workouts: newCalendarState });
    },
  componentWillMount() {
    Actions.initSessions(true);
  },

  showModal() {
    Actions.showModal('createWorkout');
  },
  render() {
    const isLoaded = this.state.workouts.isLoaded;
    let eventsNodes;
    if (isLoaded) {
      eventsNodes = this.state.workouts.data.map((item, i) => {
        return <SessionItem item={item} key={i}/>
      });
    } else {
      eventsNodes = (
        <div className="loader">
          <img src="src/img/preloader.gif" alt=""/>
        </div>
      )
    }

    const workouts = this.state.workouts.addedWorkouts;
    const addedWorkouts = this.state.workouts.addedWorkouts;

    let calendar = (workouts.length) ? calendar = <Calendar data={this.state.workouts} /> : null;

    if (addedWorkouts.length || addedWorkouts == 'empty') {
      calendar = <Calendar data={this.state.workouts} />
    }


    return(
      <div className="sessions">
        <div className="container">
	        <div className="external-events-wrap">
	          <div id="external-events" className="external-events">
	            {eventsNodes}
	          </div>
	          <div className='create-event' onClick={this.showModal}>+</div>
	        </div>
	        {calendar}
        </div>
      </div>
    );
  }
});


export default Sessions;