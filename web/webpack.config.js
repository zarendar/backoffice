var webpack = require('webpack');


var config = {
    entry: {
        javascript: "./src/js/render.js",
        html: "./index.html",
    },
    resolve: { alias: {} },
    output: {
        path: './dist',
        filename: 'app.js',
    },
    module: {
        noParse: [],
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'react-hot!babel',
            },
            {
                test: /\.html$/,
                loader: 'file?name=[name].[ext]',
            },
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader'
            },
            {
                test: /\.styl$/, loader: 'style-loader!css-loader!stylus-loader?paths=node_modules/bootstrap-styl'
            },
            {
                test   : /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
                loader : 'file-loader'
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                loaders: [
                    'file?hash=sha512&digest=hex&name=[hash].[ext]',
                    'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false'
                ]
            },
            { test: /vendor\/.+\.(jsx|js)$/,
              loader: 'imports?jQuery=jquery,$=jquery,this=>window'
            }
        ],
        plugins: [
          new webpack.HotModuleReplacementPlugin()
        ]
    }
};
module.exports = config;
